unit dothraki.obd2;

interface
uses
  System.Classes, System.Bluetooth.Components, System.Bluetooth,
  System.SysUtils, System.Variants, System.Math, dothraki.pids,
  System.Generics.Collections;

type
  TFluxoDados = (TfdEnvio, TfdRecebimento);

  TOnLogEvent = procedure(Sender: TObject; pFluxoDados: TFluxoDados;
    pDados: TArray<System.Byte>) of object;

  TOBD2Service = (osvNone, osvService1);

  TOBD2Code = class
  const
    OBD2_UUID = '{00001101-0000-1000-8000-00805F9B34FB}';

    SERVICE_01 = '01';
    ENGINE_COOLANT_TEMP = SERVICE_01+'05';
    ENGINE_SPEED =        SERVICE_01+'0C';
    VEHICLE_SPEED =       SERVICE_01+'0D';
    THROTTLE_POSITION =   SERVICE_01+'11';
    P_COMBUSTIVEL =       SERVICE_01+'23';
    FUEL_TANK =           SERVICE_01+'2F';
    DISTANCE_PERC =       SERVICE_01+'31';
    AMBIENT_AIR_TEMPERATURE =      SERVICE_01+'46';
    ACCELERATOR_PEDAL_POSITION_E = SERVICE_01+'4A';
    TIPO_COMBUSTIVEL =    SERVICE_01+'51';
    COMBUSTIVEL =         SERVICE_01+'5E';
    ODOMETER =            SERVICE_01+'A6';

    PIDS_SUPPORTED_01_20 = SERVICE_01+'00';
    PIDS_SUPPORTED_21_40 = SERVICE_01+'20';
    PIDS_SUPPORTED_11_60 = SERVICE_01+'40';
    PIDS_SUPPORTED_61_80 = SERVICE_01+'80';
  end;

  TOBD2Data = record
    NoData: boolean;
    A: integer;
    B: integer;
    C: integer;
    D: integer;
    Tamanho: integer;
  end;

  TOBD2 = class(TComponent)
  private
    FBluetooth: TBluetooth;
    FSocket: TBluetoothSocket;
    FOnConnect: TNotifyEvent;
    FOnLogEvent: TOnLogEvent;
    FSendDelay: integer;
    FPids: TObd2Pids;
    procedure EnviarDados(pObd2Code: string);
    function ReceberDados:TArray<System.Byte>;
    function ConsultarDados(pObd2Code: string):TOBD2Data;
    function BytesToOBD2Data(pDados: TArray<System.Byte>;pObd2Code: string): TOBD2Data;
    function ExecutarComando(pObd2Code: string): boolean;
    function RetornoOk(pDados: TArray<System.Byte>): Boolean;
    procedure VefificarPidsAtivos(pDados: TArray<System.Byte>;pSensor:Integer);
    function IntToBinByte(Value: Byte): string;
    function GetPids(index: string): TPidService;
    function GetPidList: TObjectDictionary<Integer, TPidService>;
  protected
    destructor Destroy; override;
  public
    constructor Create(AOwner: TComponent); override;
    function Conectado: Boolean;
    procedure Conectar(pDeviceName: String);
    procedure Desconectar;
    function EngineSpeed: Double;
    function AmbientAirTemperature: Double;
    function Odometer: Double;
    function AcceleratorPedalPositionE: Double;
    function Velocidade: Double;
    function AberturaBorboleta: Double;
    function NivelCombustivel: Double;
    function DISTANCE_PERC: Double;
    function TemperaturaMotor: Double;
    procedure VerificarPidsHabilitados(pSensor: integer);
    property OnConnect: TNotifyEvent read FOnConnect write FOnConnect;
    property OnLogEvent: TOnLogEvent read FOnLogEvent write FOnLogEvent;
    property SendDelay: Integer read FSendDelay write FSendDelay;
    property Pids[index: string]: TPidService read GetPids;
    property PidList: TObjectDictionary<Integer,TPidService> read GetPidList;
  end;



implementation


function TOBD2.DISTANCE_PERC: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.DISTANCE_PERC);

  if not lOBD2Data.NoData then
  begin
    Result := ((lOBD2Data.A * 256) + lOBD2Data.B);
  end;
end;

function TOBD2.EngineSpeed: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.ENGINE_SPEED);

  if not lOBD2Data.NoData then
  begin
    Result := ((lOBD2Data.A * 256) + lOBD2Data.B)/4;
  end;
end;

function TOBD2.TemperaturaMotor: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.ENGINE_COOLANT_TEMP);

  if not lOBD2Data.NoData then
  begin
    Result := (lOBD2Data.A - 40);
  end;
end;


function TOBD2.Odometer: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.ODOMETER);

  if not lOBD2Data.NoData then
  begin
    Result :=
         ((lOBD2Data.A*power(2,24))+
          (lOBD2Data.B*power(2,16))+
          (lOBD2Data.C*power(2,8))+
          (lOBD2Data.D))/10;
  end;
end;

function TOBD2.AberturaBorboleta: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.THROTTLE_POSITION);

  if not lOBD2Data.NoData then
  begin
    Result := (100/255)*lOBD2Data.A;
  end;
end;

function TOBD2.AcceleratorPedalPositionE: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.ACCELERATOR_PEDAL_POSITION_E);

  if not lOBD2Data.NoData then
  begin
    Result := (100/255)*lOBD2Data.A;
  end;

end;

function TOBD2.AmbientAirTemperature: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.AMBIENT_AIR_TEMPERATURE);

  if not lOBD2Data.NoData then
  begin
    Result := (lOBD2Data.A-40);
  end;
end;

function TOBD2.Velocidade: Double;
begin
  if not Conectado then
    Exit(0);
  var lOBD2Data := ConsultarDados(TOBD2Code.VEHICLE_SPEED);

  if not lOBD2Data.NoData then
  begin
    Result := (lOBD2Data.A);
  end;
end;

procedure TOBD2.VerificarPidsHabilitados(pSensor: integer);
begin
  EnviarDados(TOBD2Code.PIDS_SUPPORTED_01_20);
  VefificarPidsAtivos(ReceberDados,pSensor);

  if Pids[TOBD2Code.PIDS_SUPPORTED_21_40].Enable then
  begin
    EnviarDados(TOBD2Code.PIDS_SUPPORTED_21_40);
    VefificarPidsAtivos(ReceberDados,pSensor);
  end;

  if Pids[TOBD2Code.PIDS_SUPPORTED_11_60].Enable then
  begin
    EnviarDados(TOBD2Code.PIDS_SUPPORTED_11_60);
    VefificarPidsAtivos(ReceberDados,pSensor);
  end;

  if Pids[TOBD2Code.PIDS_SUPPORTED_61_80].Enable then
  begin
    EnviarDados(TOBD2Code.PIDS_SUPPORTED_61_80);
    VefificarPidsAtivos(ReceberDados,pSensor);
  end;
end;


function TOBD2.Conectado: Boolean;
begin
  result := (FSocket <> nil) and (FSocket.Connected);
end;


constructor TOBD2.Create(AOwner: TComponent);
begin
  FBluetooth := TBluetooth.Create(Self);
  FBluetooth.Enabled := True;
  FSendDelay := 500;
  FPids := TObd2Pids.Create;
end;

procedure TOBD2.Desconectar;
begin
  FSocket.Close;
  FreeAndNil(FSocket);
end;

destructor TOBD2.Destroy;
begin
  FSocket.Free;
  FPids.Free;
  inherited;
end;

procedure TOBD2.EnviarDados(pObd2Code: string);
var
  lDadosEnviados: TArray<System.Byte>;
begin
  lDadosEnviados := TEncoding.ANSI.GetBytes(pObd2Code+chr(13));
  if Assigned(OnLogEvent) then
    FOnLogEvent(Self,TFluxoDados.TfdEnvio,lDadosEnviados);
  FSocket.SendData(lDadosEnviados);
  Sleep(FSendDelay);
end;

function TOBD2.BytesToOBD2Data(pDados:TArray<System.Byte>; pObd2Code: string):TOBD2Data;
var
  lDados: string;
  lRetorno: TStringList;
  li: integer;
begin
  Result.NoData := True;
  Delete(pDados,Length(lDados)-3,Length(lDados));
  lRetorno := TStringList.Create;
  try
    lRetorno.Text := TEncoding.ASCII.GetString(pDados);

    pObd2Code[1] := '4';

    for li := 0 to lRetorno.Count - 1 do
    begin
      lDados := StringReplace(lRetorno[li],' ','',[rfReplaceAll]);
      if pos(pObd2Code,lDados) = 1 then
      begin
        Result.NoData := False;
        break
      end;
    end;

    if not Result.NoData then
    begin
      if Length(lDados)>=6 then
        Result.A := StrToInt('$'+lDados[5]+lDados[6]);
      if Length(lDados)>=8 then
        Result.B := StrToInt('$'+lDados[7]+lDados[8]);
      if Length(lDados)>=10 then
        Result.C := StrToInt('$'+lDados[9]+lDados[10]);
      if Length(lDados)>=12 then
        Result.D := StrToInt('$'+lDados[10]+lDados[12]);

      Result.Tamanho := (Length(lDados) div 2) - 2;
    end;
  finally
    lRetorno.Free;
  end;
end;


function TOBD2.IntToBinByte(Value: Byte): string;
var
  i: Integer;
begin
  SetLength(Result, 4);
  for i := 1 to 4 do begin
    if (Value shr (4-i)) and 1 = 0 then begin
      Result[i] := '0'
    end else begin
      Result[i] := '1';
    end;
  end;
end;


function TOBD2.NivelCombustivel: Double;
begin
  if not Conectado then
    Exit(0);

  var lOBD2Data := ConsultarDados(TOBD2Code.FUEL_TANK);

  if not lOBD2Data.NoData then
  begin
    Result := (100/255)*lOBD2Data.A;
  end;
end;

procedure TOBD2.VefificarPidsAtivos(pDados:TArray<System.Byte>;pSensor:Integer);
var
  lResultado, lDados: string;
  lRetorno: TStringList;
  lGrupo: Integer;

begin
  Delete(pDados,Length(lDados)-3,Length(lDados));
  lRetorno := TStringList.Create;
  try
    lRetorno.Text := TEncoding.ASCII.GetString(pDados);
    if pos('NO DATA',lRetorno.Text) <> 0 then
      Exit;

    lDados := lRetorno[pSensor];
    lDados := StringReplace(lDados,' ','',[rfReplaceAll]);

    lGrupo := StrToInt('$'+lDados[3]+lDados[4]);

    for var lx := 0 to 7 do
    begin
      lResultado := IntToBinByte(StrToInt('$'+lDados[5+lx]));
      for var li := 1 to Length(lResultado) do
      begin
        var lKey := lGrupo+(4*lx)+li;
        if FPids.List.ContainsKey(lKey) then
          FPids.List.Items[lKey].Enable := (lResultado[li] = '1');
      end;
    end;
  finally
    lRetorno.Free;
  end;
end;


function TOBD2.RetornoOk(pDados:TArray<System.Byte>):Boolean;
var
  lDados: string;
  lRetorno: TStringList;
begin
  Result := True;
  Delete(pDados,Length(lDados)-3,Length(lDados));
  lRetorno := TStringList.Create;
  try
    lRetorno.Text := TEncoding.ASCII.GetString(pDados);
    lDados := lRetorno[0];

    Result := pos('OK',lDados) <> 0;
  finally
    lRetorno.Free;
  end;
end;

function TOBD2.ReceberDados: TArray<System.Byte>;
var
  lRetorno: TStringList;
begin
  Result := FSocket.ReceiveData;
  lRetorno := TStringList.Create;
  try
    lRetorno.Text := TEncoding.ANSI.GetString(Result);
    if Assigned(OnLogEvent) then
      FOnLogEvent(Self,TFluxoDados.TfdRecebimento,Result);
  finally
    lRetorno.Free;
  end;
end;

procedure TOBD2.Conectar(pDeviceName: String);
var
  lDevice: TBluetoothDevice;
begin
  if Conectado then
    exit;

  for lDevice in FBluetooth.PairedDevices do
  begin
    if lDevice.DeviceName = pDeviceName then
    begin
      FreeAndNil(FSocket);
      FSocket := lDevice.CreateClientSocket(StringToGUID(TOBD2Code.OBD2_UUID),True);
      if FSocket <> nil then
      begin
        FSocket.Connect;
        EnviarDados('ATZ');
        ReceberDados;
        EnviarDados('ATE0');
        ReceberDados;
        EnviarDados('ATH0');
        ReceberDados;
        EnviarDados('ATS0');
        ReceberDados;
        EnviarDados('ATSP0');
        ReceberDados;
        if (Assigned(FOnConnect)) then
          FOnConnect(self);
      end;
    end;
  end;
end;

function TOBD2.ConsultarDados(pObd2Code: string): TOBD2Data;
begin
  if not Conectado then
    raise Exception.Create('Sem conex�o com o OBD2');

  EnviarDados(pObd2Code);
  var lDadosRecebidos := ReceberDados;
  result := BytesToOBD2Data(lDadosRecebidos, pObd2Code);
end;

function TOBD2.ExecutarComando(pObd2Code: string): boolean;
begin
  if not Conectado then
    raise Exception.Create('Sem conex�o com o OBD2');

  EnviarDados(pObd2Code);
  var lDadosRecebidos := ReceberDados;
  result := RetornoOk(lDadosRecebidos);
end;


function TOBD2.GetPidList: TObjectDictionary<Integer, TPidService>;
begin
  Result := FPids.List;
end;

function TOBD2.GetPids(index: string): TPidService;
begin
  var lKey := StrToInt('$'+index);
  if FPids.List.ContainsKey(lKey) then
    Result := FPids.List.Items[lKey];
end;

end.
